(ns frame-test.helpers)

(defmacro input-flames-helper [label value on-change]
  `[re-com/v-box
    :gap "2em"
    :justify :around
    :children [[re-com/label
                 :label ~label]
                [re-com/input-text
                 :model ~value
                 :width "200px"
                 :change-on-blur? false
                 :on-change ~on-change]]])
