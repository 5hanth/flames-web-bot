(ns frame-test.handlers
    (:require [re-frame.core :as re-frame]
              [frame-test.db :as db]))

(re-frame/register-handler
 :initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/register-handler
 :set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(re-frame/register-handler
 :set-names
 (fn [db [_ {:keys [your-name other-name]}]]
   (assoc db :names {:your-name
                     (or your-name
                         (:your-name (:names db)))
                     :other-name
                     (or other-name
                         (:other-name (:names db)))})))
