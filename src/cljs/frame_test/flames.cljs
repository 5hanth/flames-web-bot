(ns frame-test.flames)

(def flames-map {"F" "\uD83D\uDE09 Friend \uD83D\uDE09"
                 "L" "\u2665 Love \u2665" "A" "\u2698 Affection \u2698"
                 "M" "\u2766 Marriage \u2766" "E" "\u27B3 Enemy \u27B3"
                 "S" "\u263A Sister \u263A"})

(defn _do-flames [n fl]
      (let [do-split (fn [n fl]
			 (let [[xs ys] (split-at n fl)]
			      (flatten
			       (conj (butlast xs)
				     ys))))
	   c (count fl)]
	   (cond (= c 1)
		 (first fl)
		 (<= n c)
		 (_do-flames n (do-split n fl))
		 :else (let [n-n (let [r (rem n c)]
				      (if (= r 0) n r))
			    n-f (do-split n-n fl)]
			    (_do-flames n n-f)))))
			   
(defn find-freq [n1 n2]
      (let [lns (map clojure.string/lower-case [n1 n2])
	    sns (map #(remove (fn [c] (= \space c)) %1) lns)]
	   (map frequencies sns)))
	  
(defn flames-number [n1 n2]
      (let [[m1 m2] (find-freq n1 n2)
	    f-val (reduce + (for [[k v]
			(merge-with #(Math/abs (- % %2))
				    m1 m2 )
			:when (not= 0 v)] v))]
        (if (zero? f-val) 1 f-val)))

(defn __do-flames [n1 n2]
      (_do-flames (flames-number n1 n2) (map str "FLAMES")))

(defn do-flames [n1 n2]
  (flames-map (__do-flames n1 n2)))
