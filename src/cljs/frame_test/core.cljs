(ns frame-test.core
    (:require [reagent.core :as reagent]
              [re-frame.core :as re-frame]
              [frame-test.handlers]
              [frame-test.subs]
              [frame-test.routes :as routes]
              [frame-test.views :as views]
              [frame-test.config :as config]))

(when config/debug?
  (println "dev mode"))

(defn mount-root []
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init [] 
  (routes/app-routes)
  (re-frame/dispatch-sync [:initialize-db])
  (mount-root))
