(ns frame-test.views
    (:require [re-frame.core :as re-frame]
              [re-com.core :as re-com]
              [frame-test.flames :as f])
    (:require-macros [frame-test.helpers :as h]))

;; flames form
(defn input-flames  []
  (let [names (re-frame/subscribe [:names])]
    (fn []
      [re-com/v-box
       :gap "2em"
       :children [(h/input-flames-helper "Your name"
                   (:your-name @names)
                   #(re-frame/dispatch [:set-names
                                        {:your-name (.join (.split % " ") "")}]))
                  
                  (h/input-flames-helper "Your crush name"
                   (:other-name @names)
                   #(re-frame/dispatch [:set-names
                                        {:other-name (.join (.split % " ") "")}]))]])))

(defn flames-display []
  (let [names (re-frame/subscribe [:names])]
     [:h3.text-center.text-success (if (not 
             (some true?
                     (map empty? [(:your-name @names)
                                  (:other-name @names)])))
              (str (:your-name @names) " "
                   (f/do-flames (:your-name @names)
                                (:other-name @names)) " "
                   (:other-name @names))
              "Wanna find Flames with your crush ?")]))

;; home

(defn home-panel []
  [re-com/v-box
   :align :center
   :gap "3em"
   :children [[flames-display]
              [input-flames]]])


(defn navbar []
  [re-com/v-box
   :align-self :stretch
   :children [[re-com/h-box
               :justify :between
               :align :center
               :margin "0em 1em 0em 1em"
               :children [[:a.navbar-brand {:href "#/"} "Flames Bot"]
                          [:a.navbar-btn {:href "https://5hanth.github.io"} "@5hanth"]]]
              
              [re-com/line]
              [re-com/gap :size "2em"]]])




;; main

(defmulti panels identity)
(defmethod panels :home-panel [] [home-panel])
(defmethod panels :default [] [:div])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [:active-panel])]
    (fn []
      [re-com/v-box
       :align :center
       :height "100%"
       :children [[navbar]
                  (panels @active-panel)]])))
